package edu.uprm.cse.datastructures.cardealer.model;


import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
//import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarTable {

	private static HashTableOA<Integer, Car> list2 = new HashTableOA<>(new CarComparator());

	public CarTable() {}

	public static HashTableOA<Integer, Car> getInstance() { return list2; }

	public static Car[] getCarArray() {	
		Car[] cars = new Car[list2.size()];
		SortedList<Car> carsV = list2.getValues();
		
		for (int i = 0; i < cars.length; i++)
			cars[i] = carsV.get(i);
		return cars;
	}

	public static void resetCars() { CarTable.getInstance().clear(); }
}
