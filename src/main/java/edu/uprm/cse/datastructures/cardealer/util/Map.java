package edu.uprm.cse.datastructures.cardealer.util;

import java.io.PrintStream;

public interface Map<K,V>{
	
	public int size();
	public boolean isEmpty();
	public boolean contains(K key);
	
	public V get(K key);
	public V put(K key, V value);
	public V remove(K key);
	
	public SortedList<K> getKeys();
	public SortedList<V> getValues();
	public void clear();
	public void print(PrintStream out);
	
}
