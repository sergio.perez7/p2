package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;

@Path("/cars")
public class CarManager {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() { return CarTable.getCarArray(); }

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") int id) {
		Car car = CarTable.getInstance().get((int) id);

		if (car == null) {
			System.out.println("Car not found in getCar().");
			throw new NotFoundException("Error Car " + id + " not found");
		} else
			return car;
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {

		if (car == null)
			throw new IllegalArgumentException("Car cannot be null");

		CarTable.getInstance().put((int) car.getCarId(), car);
		System.out.println("POST: " + car);
		return Response.status(Response.Status.CREATED).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Car car) {

		if (car == null)
			throw new IllegalArgumentException("Car cannot be null");

		CarTable.getInstance().put((Integer) (int) car.getCarId(), car);
		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("{id}/delete")
	public Response delete(@PathParam("id") int id) {

		Car car = getCar(id);
		if (car == null) {
			System.out.println("Car not found in delete().");
			return Response.status(Response.Status.NOT_FOUND).build();
		} else {
			CarTable.getInstance().remove((int) car.getCarId());
			return Response.status(Response.Status.OK).build();
		}
	}
}