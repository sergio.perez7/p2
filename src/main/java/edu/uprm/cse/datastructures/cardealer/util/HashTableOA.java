package edu.uprm.cse.datastructures.cardealer.util;

import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.Iterator;

public class HashTableOA<K,V> implements Map<K,V>{

	private static class Bucket<K,V>{
		private K key;
		private V value;
		private boolean inUse;
		
		public Bucket() { super(); }
		public Bucket(K key, V value, boolean inUse) {
			super();
			this.key = key;
			this.value = value;
			this.inUse = inUse;
		}

		
		public K getKey() { return key; }
		public V getValue() { return value; }
		public boolean isInUse() { return inUse; }
		public void setKey(K key) { this.key = key; }
		public void setValue(V value) { this.value = value; }
		public void setInUse(boolean inUse) { this.inUse = inUse; }
		public void setBucket(K key, V value, boolean used)  {setKey(key); setValue(value); setInUse(used); }
	}

	private Object[] buckets;
	private int currentSize;
	private Comparator<V> comp;
	private Comparator<K> compKey;
	private static final int DEFAULT_SIZE = 11;

	public HashTableOA(int iniCapacity) {
		
		if(iniCapacity <= 0)
			throw new IllegalArgumentException("Initial capacity cannot be <= 0.");
		if(iniCapacity >= 0 && iniCapacity < 5) {
			iniCapacity = 5;
			System.out.println("Minimum capacity is 5."); // Because of hashFunction2().
		}
			
		currentSize = 0;
		buckets = new Object[getPrime(iniCapacity)];
		for (int i = 0; i < buckets.length; i++)
			buckets[i] = new Bucket<K,V>(null,null,false);
	}
	
	public HashTableOA() 											{ this(DEFAULT_SIZE); }
	
	public HashTableOA(Comparator<V> comp) 							{ this(DEFAULT_SIZE); this.comp = comp; }
	
	public HashTableOA(Comparator<V> comp, Comparator<K> keyComp) 	{ this(DEFAULT_SIZE, comp); this.compKey = keyComp;} 
	
	public HashTableOA(int iniCapacity, Comparator<V> comp) 		{ this(iniCapacity); this.comp = comp; }
	
	private int getPrime(int n) {
		BigInteger b = new BigInteger(Integer.toString(n));
		return b.isProbablePrime(1) ? n : Integer.parseInt(b.nextProbablePrime().toString());
	}
	
	private int hashFunction1(K key) 	{ return key.hashCode()*3 % buckets.length; }
	
	private int hashFunction2(K key) 	{ return 3 - (hashFunction1(key) % 3); }
	
	@Override
	public int size() 				{ return currentSize; }

	@Override
	public boolean isEmpty() 		{ return size() == 0; }

	@Override
	public boolean contains(K key) 	{ return get(key) != null; }

	@Override
	public V get(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null.");
		
		int target = hashFunction1(key);
		Bucket<K,V> B = (Bucket<K, V>) buckets[target];
		
		if (B.isInUse() && B.getKey().equals(key))
			return B.getValue();
		else {
			target = (target + hashFunction2(key)) % buckets.length;
			B = (Bucket<K,V>)buckets[target];
						
			return (B.isInUse() && B.getKey().equals(key)) ? B.getValue() : getLinearProbing(target,key);
		}
	}
	
	private V getLinearProbing(int start, K key) {		
		int n = buckets.length;
		Bucket<K,V> B = null;
		
		for(int i = (start+1) % n; i != start; i = (i + 1) % n) {
			B = (Bucket<K, V>) buckets[i];
			if(B.isInUse() && B.getKey().equals(key))
				return B.getValue();
		}
		return null;
	}
	
	@Override
	public V put(K key, V value) {
		if(key == null || value == null)
			throw new IllegalArgumentException("Key or value cannot be nulll");
		V oldVal = this.remove(key);
		if(size() == buckets.length)
			reAllocate();
		
		int target = hashFunction1(key);
		Bucket<K,V> B = (Bucket<K,V>)buckets[target];
	
		if(!B.isInUse()) { B.setBucket(key, value, true); currentSize++; }

		else {
			target = (target + hashFunction2(key)) % buckets.length;
			B = (Bucket<K,V>)buckets[target];
			
			if(!B.isInUse()) {  B.setBucket(key, value, true); currentSize++; }
			else 			 {	putLinearProbing(target,key,value); }
		}
		return oldVal;
	}
	
	private void reAllocate() {
		
		Bucket<K,V> B = null;
		Object[] oldTable = buckets;
		buckets = new Object[size()*2];
		currentSize = 0;
		
		for (int i = 0; i < buckets.length; i++)
			buckets[i] = new Bucket<K,V>(null,null,false);
		
		for (int i = 0; i < oldTable.length; i++) {
			B = (Bucket<K, V>) oldTable[i];
			put(B.getKey(), B.getValue());
		}
	}
	
	private void putLinearProbing(int start, K key, V value) {
		
		int n = buckets.length;
		Bucket<K,V> B = null;
		
		for(int i = (start + 1) % n; i != start; i = (i + 1) % n) {
			B = (Bucket<K, V>) buckets[i];
			if(!B.isInUse()) {
				B.setBucket(key, value, true);
				currentSize++;
			}
		}
	}
	
	@Override
	public V remove(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null.");
		
		int target = hashFunction1(key);
		Bucket<K,V> B = (Bucket<K, V>) buckets[target];
		
		if(B.isInUse() && B.getKey().equals(key)) {
			V oldValue = B.getValue();
			B.setBucket(null, null, false);
			currentSize--;
			return oldValue;
		}
		else {
			target = (target + hashFunction2(key)) % buckets.length;
			B = (Bucket<K,V>)buckets[target];
			
			if(B.isInUse() && B.getKey().equals(key)) {
				V oldValue = B.getValue();
				B.setBucket(null, null, false);
				currentSize--;
				return oldValue;
			}
			else
				return removeLinearProbing(target,key);
		}
	}
	
	private V removeLinearProbing(int start, K key) {
		int n = buckets.length;
		Bucket<K,V> B = null;
		
		for(int i = (start+1)%n; i != start; i = (i+1)%n) {
			B = (Bucket<K, V>) buckets[i];
			if(B.isInUse() && B.getKey().equals(key)) {
				V oldValue = B.getValue();
				B.setBucket(null, null, false);
				currentSize--;
				return oldValue;
			}	
		}
		return null;
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> result = new CircularSortedDoublyLinkedList<K>();
		Bucket<K,V> B = null;
		
		for (int i = 0; i < buckets.length; i++) {
			B = (Bucket<K, V>) buckets[i];
			if(B.isInUse())
				result.add(B.getKey());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SortedList<V> getValues() {
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>(comp);
		Bucket<K,V> B = null;
		
		for (int i = 0; i < buckets.length; i++) {
			B = (Bucket<K, V>) buckets[i];
			if(B.isInUse())
				result.add(B.getValue());
		}
		return result;
	}
	
	@Override
	public void clear() {
		Bucket<K,V> B = null;
		currentSize = 0;
		
		for (int i = 0; i < buckets.length; i++) {
			B = (Bucket<K,V>)buckets[i];
			B.setBucket(null, null, false);
		}
	}

	@Override
	public void print(PrintStream out) {
		Bucket<K,V> B = null;
		for (int i = 0; i < buckets.length; i++) {
			B = (Bucket<K,V>)buckets[i];
			if(B.isInUse())
				out.printf("(%s, %s)\n", B.getKey(), B.getValue());
		}
	}
}
